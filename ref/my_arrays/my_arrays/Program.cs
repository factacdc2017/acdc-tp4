﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace my_arrays
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] tab = new int[7] {6,2,2,3,4,5,1};
            int min = 0;
            int max = 0;

            Console.WriteLine(mintab(tab, ref min));
            Console.WriteLine(maxtab(tab, ref max));

            bubblesort(tab);

            for (int i = 0 ; i < tab.Length ; i++)
            {
                Console.WriteLine(tab[i]);
            }

        }

        static int mintab(int[] tab, ref int min)
        {
            int n = tab.Length;
            min = tab[0];

            for ( int i = 0 ; i < n ; i++)
            {
                if (tab[i] < min)
                {
                    min = tab[i];
                }
            }
            return min;
        }

        static int maxtab(int[] tab, ref int max)
        {
            int n = tab.Length;
            max = tab[0];

            for (int i = 0; i < n; i++)
            {
                if (tab[i] > max)
                {
                    max = tab[i];
                }
            }
            return max;
        }

        static void swap(ref int a, ref int b)
        {
            int c = 0;
            c = a;
            a = b;
            b = c;
        }

        static void bubblesort(int[] tab)
        {
            int n = tab.Length;
            bool no_change;

            for (int i = n-1; i > 0 ; i--)
            {
                no_change = true;

                for (int j = 0 ; j<i; j++)
                {
                    if (tab[j]> tab[j+1])
                    {
                        swap(ref tab[j], ref tab[j+1]);
                        no_change = false;
                    }
                }

                if (no_change)
                {
                    return;
                }

            }
        }

    }
}

