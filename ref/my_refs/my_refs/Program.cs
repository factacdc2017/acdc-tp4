﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Schema;

namespace my_refs
{
    public class Program
    {
        static void Main(string[] args)
        {
            float result = 0.2F;
            add(ref result, 4.0F);
            Console.WriteLine(result);

            sub(ref result, 0.2F);
            Console.WriteLine(result);

            prod(ref result, 0.3F);
            Console.WriteLine(result);

            div(ref result, 0.4F);
            Console.WriteLine(result);

            result = 4F;

            Console.WriteLine(pow(ref result, -2));
            Console.WriteLine(result);

            float Un = 0;
            arit(ref Un , 1 , 5);
            Console.WriteLine(Un);

            Un = 1;
            geom(ref Un, 2,2);
            Console.WriteLine(Un);

            int a = 1;
            int b = 2;

            swap(ref a, ref b);
            Console.WriteLine(a);
            Console.WriteLine(b);
        }

        public static void add(ref float result, float value)
        {
            result = result + value;
        }

        static void sub(ref float result, float value)
        {
            result = result - value;
        }

        static void prod(ref float result, float value)
        {
            result = result*value;
        }

        static bool div(ref float result, float value)
        {
            if (value == 0)
            {
                return false;
            }
            else
            {
                result = result/value;
                return true;
            }
        }

        static bool pow(ref float result, int pow)
        {
            float n = 1;

            if (result == 0 && pow < 0)
            {
                return false;
            }
            else if (pow > 0)
            {
                if (result == 0)
                {
                    return true;
                }

                else
                {
                    for (int i = 0; i < pow; i ++)
                    {
                        n = n*result;
                    }
                    result = n;
                }
                return true;
            }

            else
            {
                pow = -pow;

                if (result == 0)
                {
                    return false;
                }
                else
                {
                    for (int i = 0; i < pow; i++)
                    {
                        n = n * result;
                    }
                    result = 1/n;
                }

                return true;
            }
        }

        static void arit(ref float Un, float r, int n)
        {
            Un = Un + r*n;
        }

        static void geom(ref float Un, float r , int n)
        {
            Un = Un*(float)Math.Pow(r, n);
        }

        static void swap(ref int a, ref int b)
        {
            int c = a;
            a = b;
            b = c;
        }
    }
}
