﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace my_refs
{
    class Program
    {
        static void add(ref float result, float value)
        {
            result = result + value;
        }
        static void sub(ref float result, float value)
        {
            result = result - value;
        }
        static void mul(ref float result, float value)
        {
            result = result * value;
        }
        static bool div(ref float result, float value)
        {
            if (value == 0)
            {
                return false;
            }
            else
            {
                result = result / value;
                return true;
            }

        }
        static bool pow(ref float result, int p)
        {
            if (p > 0)
            {
                {
                    float n = 1.0f;
                    for (int i = 1; i <= p; i++)
                    {
                        n = (n * result);

                    }
                    result = n;

                }
                return true;
            }
            else
            {

                return false;
            }
        }
        static void arit(ref float Un, float r, int n)
        {
            Un = Un + (n - 2) * r;
        }
        static void geom(ref float Un, float q, int n)
        {
            float j = 1.0f;
            for (int i = 1; i <= (n - 2); i++)
            {
                j = j * q;
            }
            q = j;
            Un = Un * q;
        }
        static void swap(ref int a, ref int b)
        {
            int c;
            c = a;
            a = b;
            b = c;
        }
        static void Main(string[] args)
        {
            float result = 9.0f;
            add(ref result, 3.5f);
            Console.WriteLine(result);
            result = 9.0f;
            sub(ref result, 4.5f);
            Console.WriteLine(result);
            result = 9.0f;
            mul(ref result, 2.5f);
            Console.WriteLine(result);
            result = 9.0f;
            div(ref result, 4.5f);
            Console.WriteLine(result);
            result = 9.0f;
            pow(ref result, 4);
            Console.WriteLine(result);
            float Un = 2.0f;
            arit(ref Un, 4.0f, 6);
            Console.WriteLine(Un);
            Un = 2.0f;
            geom(ref Un, 7.0f, 5);
            Console.WriteLine(Un);
            int a = 6;
            int b = 12;
            swap(ref a, ref b);
            Console.WriteLine(a + "," + b);
        }
    }
}
