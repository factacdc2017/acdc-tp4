﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace my_arrays
{
    class Program
    {
        static int mintab(int[] tab, ref int min)
        {
            int position = 0;

            for (int i = 0; i < tab.Length; i++)
            {
                if (min > tab[i])
                {
                    min = tab[i];
                    position = i;
                }


            }
            return position;
        }
        static int maxtab(int[] tab, ref int max)
        {
            int position = 0;

            for (int i = 0; i < tab.Length; i++)
            {
                if (max < tab[i])
                {
                    max = tab[i];
                    position = i;
                }


            }
            return position;
        }

        /*static void bubblesort(int[] tab)
        {
            for (int i = 0; i < tab.Length; i++)
            {

                if (tab[i] > tab[i + 1])
                {
                    swap(ref tab[i], ref tab[i + 1]);
                }
            }

        }*/

        static void Main(string[] args)
        {
            int[] tab;
            tab = new int[5] { 2, 5, 3, 8, 1 };
            int min = 3;
            Console.WriteLine(mintab(tab, ref min));
            int max = 3;
            Console.WriteLine(mintab(tab, ref max));
            tab = new int[5] { 2, 5, 8, 3, 1 };
            Console.WriteLine(tab);
        }
    }
}
