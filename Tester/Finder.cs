﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

using Fact.Processing;

using Function = System.Tuple<
    System.Type, System.Type, System.Reflection.MethodInfo>;
using System.Reflection.Emit;

namespace Tester
{
    class Finder {

        public Assembly a { get; private set; }

        private string _TempFolder = "";
        private string assembly_name;

        private Dictionary<string, List<Function>> methods = new Dictionary<string, List<Function>>();

        public Finder(Project student, string folderHint = "/") {
            List<File> e = student.GetFiles(folderHint, true, Fact.Processing.File.FileType.Executable);

            List<File> exes = e.Where(
                x => !x.Name.Contains("vshost") && !x.Directory.Contains("obj")
             ).ToList();

            File f = null;
            if (exes.Count > 1) f = exes[Ask(exes)];
            else if (e.Count == 0) Fact.Assert.Basic.MustNotOccurs();
            else f = exes[0];

            make_temp();
            assembly_name = _TempFolder + "\\" + f.Name;

            f.ExtractAt(_TempFolder);
            a = Assembly.Load(System.IO.File.ReadAllBytes(assembly_name));

            explore();
        }

        public void Cleanup()
        {
            Fact.Tools.RecursiveDelete(_TempFolder);
            Fact.Assert.IO.DirectoryNotExists(_TempFolder);
        }


        private void make_temp() {
            _TempFolder = Fact.Tools.CreateTempDirectory();
            Fact.Assert.IO.DirectoryExists(_TempFolder);
        }

        private int Ask<T>(List<T> os) {
            for (int i = 0; i < os.Count; ++i)
                Console.WriteLine("{0}: {1}", i, os[i].ToString());
            int result = -1;
            do {
                Console.Write("Choice: ");
                Int32.TryParse(Console.ReadLine(), out result);
                if (result < 0 || result >= os.Count)
                    result = -1;
            } while (result == -1);

            return result;
        }

        private void insert(Type namespace_, Type class_, string name, MethodInfo method) {
            if (!methods.ContainsKey(name))
                methods[name] = new List<Function>();
            methods[name].Add(new Function(namespace_, class_, method));
        }

        public MethodInfo get(string name) {
            if (!methods.ContainsKey(name))
                Fact.Assert.Basic.IsTrue(false, Fact.Assert.Assert.Trigger.FATAL, "Method Not Found");

            if (methods[name].Count > 1)
                return methods[name][Ask(methods[name])].Item3;
            return methods[name][0].Item3;
        }

        private void explore() {
            Type[] namespaces = a.GetTypes().Where(x => !x.FullName.Contains("PrivateImplementationDetails")).ToArray();
            foreach (Type n in namespaces) {
                Type[] classes = a.GetTypes().Where(
                    t => String.Equals(t.Namespace, n.Namespace, StringComparison.Ordinal)
                ).ToArray();

                foreach (Type c in classes)
                    foreach (var method in c.GetMethods(BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public)) {
                        insert(n, c, method.Name, method);
                    }   
            }
        }

        private void listNamespaces() {
            foreach (Type t in a.GetTypes())
                if (!t.FullName.Contains("PrivateImplementationDetails"))
                    Console.WriteLine(t.FullName);
            
        }
    }
}
