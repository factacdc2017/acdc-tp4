﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Reflection;
using System.IO;

namespace Tester
{
    [Fact.Attribute.Test.Group("Reference")]
    public class RefTester
    {
        Finder f;

        [Fact.Attribute.Test.Setup("Init")]
        public void Setup(Fact.Processing.Project student) {
            f = new Finder(student, "/my_refs");
        }

        public static bool equal(float a, float b) { return Math.Abs(a - b) < 0.1; }

        [Fact.Attribute.Test.Test("Addition")]
        public void addTester() {

            Object[] args = new Object[] { 3f, 3f };
            f.get("add").Invoke(obj: null, parameters: args);

            Fact.Assert.Basic.IsTrue(equal((float) args[0], 6));
        }

        [Fact.Attribute.Test.Test("Substraction")]
        public void subTester() {

            Object[] args = new Object[] { 4f, 3f };
            f.get("sub").Invoke(obj: null, parameters: args);

            Fact.Assert.Basic.IsTrue(equal((float)args[0], 1));
        }

        [Fact.Attribute.Test.Test("Product")]
        public void prodTester() {

            Object[] args = new Object[] { 2f, 3f };
            f.get("prod").Invoke(obj: null, parameters: args);

            Fact.Assert.Basic.IsTrue(equal((float)args[0], 6));
        }

        [Fact.Attribute.Test.Test("Division")]
        public void divTester() {

            Object[] args = new Object[] { 8f, 2f };
            f.get("div").Invoke(obj: null, parameters: args);

            Fact.Assert.Basic.IsTrue(equal((float)args[0], 4));
        }

        [Fact.Attribute.Test.Test("Power")]
        public void powTester() {

            bool ret;
            Object[] args = new Object[] { 2f, 5 };

            ret = (bool) f.get("pow").Invoke(obj: null, parameters: args);
            Fact.Assert.Basic.IsTrue(equal((float)args[0], 32));
            Fact.Assert.Basic.IsTrue(ret);

            args = new Object[] { 2f, -5 };

            ret = (bool)f.get("pow").Invoke(obj: null, parameters: args);
            Fact.Assert.Basic.IsTrue(equal((float)args[0], 1 / 32f));
            Fact.Assert.Basic.IsTrue(ret);

            ret = (bool)f.get("pow").Invoke(obj: null, parameters: new Object[] { 0f, 0 });
            Fact.Assert.Basic.IsFalse(ret);
        }

        [Fact.Attribute.Test.Test("Arith Sequence")]
        public void arithTester() {

            Object[] args = new Object[] { 2f, 3, 5 };
            f.get("arit").Invoke(obj: null, parameters: args);

            Fact.Assert.Basic.IsTrue(equal((float)args[0], 2 + 3 * 5));
        }

        [Fact.Attribute.Test.Test("Geom Sequence")]
        public void geomTester() {

            Object[] args = new Object[] { 2f, 2, 5 };
            f.get("geom").Invoke(obj: null, parameters: args);

            Fact.Assert.Basic.IsTrue(equal((float)args[0], 2 * 32));
        }

        [Fact.Attribute.Test.Teardown("Teardown environement")]
        public void Teardown() {
            f.Cleanup();
        }
    }
}
