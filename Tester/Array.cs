﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Tester {
    [Fact.Attribute.Test.Group("Array")]
    class Array {


        Finder f;

        [Fact.Attribute.Test.Setup("Init")]
        public void Setup(Fact.Processing.Project student) {
            f = new Finder(student, "/my_arrays");
        }

        [Fact.Attribute.Test.Test("Min Tab")]
        public void minTester() {

            Object[] args = new Object[] { new int[7] { 6, 2, 2, 3, 4, 5, 1 }, 2 };

            f.get("mintab").Invoke(obj: null, parameters: args);

            Fact.Assert.Basic.AreEqual(1, (int)args[1]);
        }

        [Fact.Attribute.Test.Test("Max Tab")]
        public void maxTester() {

            Object[] args = new Object[] { new int[7] { 6, 2, 2, 3, 4, 5, 1 }, 2 };
            f.get("maxtab").Invoke(obj: null, parameters: args);
            Fact.Assert.Basic.AreEqual(6, (int) args[1]);
        }

        [Fact.Attribute.Test.Test("Bubblesort")]
        public void bubbleTester() {

            Object[] args = new Object[] { new int[7] { 6, 2, 2, 3, 4, 5, 1 } };
            int[] result = (int[]) args[0];
            f.get("bubblesort").Invoke(obj: null, parameters: args);

            int[] reference = new int[7] { 1, 2, 2, 3, 4, 5, 6 };
            for (int i = 0; i < 7; ++i)
                Fact.Assert.Basic.AreEqual(reference[i], result[i]);
        }

        [Fact.Attribute.Test.Teardown("Teardown environement")]
        public void Teardown() {
            f.Cleanup();
        }

    }
}
